﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VSJira.Core
{
    public class ThemeManager
    {
        private VSJiraTheme? _theme = null;
        private readonly ResourceDictionary _resources;
        private static List<string> _sharedStyles = new List<string>()
        {
            "pack://application:,,,/MahApps.Metro;component/Styles/Controls.xaml",
            "pack://application:,,,/MahApps.Metro;component/Styles/Fonts.xaml",
            "pack://application:,,,/MahApps.Metro;component/Styles/Colors.xaml",
            "pack://application:,,,/MahApps.Metro;component/Styles/Accents/Blue.xaml",
            "pack://application:,,,/VSJira.Core;component/Styles/CheckComboBox.xaml"
        };

        public ThemeManager(ResourceDictionary resources, VSJiraTheme theme = VSJiraTheme.Light)
        {
            this._resources = resources;
            Apply(theme);
        }

        public void Apply(VSJiraTheme theme)
        {
            if (!_theme.HasValue || _theme.Value != theme)
            {
                _theme = theme;
                ThemeManager.Apply(this._resources, theme);
            }
        }

        public static void Apply(ResourceDictionary resources, VSJiraTheme theme)
        {
            resources.MergedDictionaries.Clear();

            // add shared styles
            foreach (var style in _sharedStyles)
            {
                resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(style, UriKind.RelativeOrAbsolute) });
            }

            // add theme
            var themeSource = String.Format("pack://application:,,,/MahApps.Metro;component/Styles/Accents/Base{0}.xaml", theme.ToString());
            resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(themeSource, UriKind.RelativeOrAbsolute) });
        }
    }
}
