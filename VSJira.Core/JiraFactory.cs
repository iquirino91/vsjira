﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class JiraFactory : IJiraFactory
    {
        public Jira Create(string url, string username, string password)
        {
            return Jira.CreateRestClient(url, username, password);
        }
    }
}
