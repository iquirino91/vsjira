﻿// Guids.cs
// MUST match guids.h
using System;

namespace VSJira.Package
{
    static class GuidList
    {
        public const string guidVSJiraPkgString = "5e8d215b-bdef-4bd5-bee5-a8317e052bfb";
        public const string guidVSJiraCmdSetString = "71eecd07-79a4-42c1-8516-e49ca7fd2a8e";
        public const string guidJiraIssuesToolWindow = "df49c318-439c-48fe-be08-d44143d2598b";
        public const string guidJiraIssueToolWindow = "3AAC7898-25BD-4A8A-981C-1237C9DC3E63";

        public static readonly Guid guidVSJiraCmdSet = new Guid(guidVSJiraCmdSetString);
    };
}